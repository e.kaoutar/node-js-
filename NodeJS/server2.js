


let app = require('express')()

//lorsque tu rencontres la racine : ça prendra un callback avec une fonction
app.get('/', (req, rep) => {

    //au lieu de response.end ==> response.send
    rep.send('Salut tu es à la racine')
})


//appel d'une autre page pareil
app.get('/demo', (req, rep) => {
    rep.send('Salut tu es sur la demo')
})


//enfin on va mentionner sur quoi il doit ecouter :
app.listen(80)











/*
let _ = require('lodash')


console.log(_.map([1, 2, 3], function(n){return n*3}))

let app = require('./app').start(80)
app.on('root', function(response){
    response.end('Je suis la racine')
})
*/