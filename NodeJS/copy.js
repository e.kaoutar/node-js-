//pour faire une copie de la video jointe : LECTURE PAR MORCEAUX
/* On commence par importer fs
*/

let fs = require('fs')

let file = 'music.mp4'



fs.stat(file, (err, stat) => {
    //j'ai besoin de connaitre la taille de mon fichier
    let total = stat.size

    //j'ai besoin de connaitre l'etat de progression de mon fichier
    let progress=0 //par defaut ça sera 0

    //lecture
    let read = fs.createReadStream(file)
    //ecriture
    let write = fs.createWriteStream('copy.mp4')

    //on peut aussi une deuxieme ecriture pour la mm lecture
    let write2 = fs.createWriteStream('copy2.mp4')

    //lorsque tu reçois des donnees tu prendras en parametres une fonction qui prendra en parametre un chunk
    ///un chunk = soit buffer soit string
    ////ici chunk est un buffer
    /////chunk = lecture par morceaux selon la taille (length)
    read.on('data', (chunk) => {

        //on cumule le progret :
        progress+=chunk.length

        //a chaque fois qu'il va recevoir des donnees du fichier a lire, il va afficher le morceau qu'il a lu
        console.log("J'ai lu " + Math.round(100 * progress/total) + "% du fichier")
    })

    //le pipe gère la connection entre le reading du fichier et le writing avec les pauses de lectures et tout
    read.pipe(write)
    read.pipe(write2)

    //marquer la fin de l'ecriture
    write.on('finish', () => {
        console.log("Le fichier a bien été copié")
    })
})



