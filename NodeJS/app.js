
//il est indispensable de declarer ces variables avant d'executer sinon ce module ne fonctionnera pas tant que les eventemmiters ne sont pas là
let http = require('http')
let fs = require('fs')
let url = require('url')
const EventEmitter = require('events')



let App = {
    start : function(port)
    {
        //on cree un listener
        let emmeteur = new EventEmitter()

        //on demarre notre serveur:
        let serveur = http.createServer((request, response) => {
            //callback
            ///on veut tjr envoyer un entete de 200
            response.writeHead(200)
            ///detection de la racine : on arrive a la racine quand l'url qui ete demande c la racine
            if(request.url === '/')
            {
                //envoi a l'evenement la racine et la reponse aussi
                emmeteur.emit('root', response)
            }

            //on veut tjr ecrire quoi qu'il arrive
            response.end()

        }).listen(port)

        //retourner le listener a la fin
        return emmeteur

    }
  }

    module.exports = App